# 1 Create database "myshop"
CREATE DATABASE myshop;


# 2 Create table of users, items, and categories in myshop database
tbl. users:
CREATE TABLE `users` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) DEFAULT NULL,
 `email` varchar(255) DEFAULT NULL,
 `password` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4

tbl. items:
CREATE TABLE `items` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `category_id` int(11) DEFAULT NULL,
 `name` varchar(255) DEFAULT NULL,
 `description` varchar(255) DEFAULT NULL,
 `price` int(11) DEFAULT NULL,
 `stock` int(11) DEFAULT NULL,
 PRIMARY KEY (`id`),
 KEY `fk_category_id_categories` (`category_id`),
 CONSTRAINT `fk_category_id_categories` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4

tbl. categories
CREATE TABLE `categories` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `name` varchar(255) DEFAULT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4


# 3 Insert data into users, categories, and items tables
INSERT INTO categories(name)
VALUES('gadget');
INSERT INTO categories(name)
VALUES('cloth');
INSERT INTO categories(name)
VALUES('men');
INSERT INTO categories(name)
VALUES('women');
INSERT INTO categories(name)
VALUES('branded');

INSERT INTO users(name,email,password)
VALUES('John Doe','john@doe.com','john123');
INSERT INTO users(name,email,password)
VALUES('Jane Doe','jane@doe.com','jenita123');

INSERT INTO items(name,description,price,stock,category_id)
VALUES('Sumsang b50','hape keren dari merek sumsang',4000000,100,1);
INSERT INTO items(name,description,price,stock,category_id)
VALUES('Uniklooh','baju keren dari brand ternama',500000,50,2);
INSERT INTO items(name,description,price,stock,category_id)
VALUES('IMHO Watch','jam tangan anak yang jujur banget',2000000,10,1);


# 4 Select data from table
a. Select data from users table without password column
SELECT u.id, u.name, u.email FROM users u;

b. Select data from items table with conditional 
SELECT * FROM items i where i.price > 1000000;
SELECT * FROM items i where i.name like '%sang%';

c. Select data from items and categories to get categories name
SELECT i.*, c.name FROM items i LEFT JOIN categories c ON i.category_id = c.id;


# 5 Update data price from items table 
UPDATE items SET price = 2500000 WHERE name like '%sumsang b50%';