<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>OOP Exercise</title>
    <style>
        .container {
            margin: 0 auto;
            padding: 10px;
            font-family: Arial;
        };
    </style>
</head>
<body>
    <div class="container">
    <?php 
        require "animal.php";
        require "frog.php";
        require "ape.php";

        $sheep = new Animal("shaun");
        echo "Name : $sheep->name <br>"; // "shaun"
        echo "Legs : $sheep->legs <br>"; // 4
        echo "Cold Blooded : $sheep->cold_blooded <br>"; // "no"

        echo "<br>";

        $kodok = new Frog("buduk");
        echo "Name : $kodok->name <br>"; 
        echo "Legs : $kodok->legs <br>"; 
        echo "Cold Blooded : $kodok->cold_blooded <br>"; 
        echo "Jump : " . $kodok->jump() . "<br>"; // "hop hop"
        
        echo "<br>";

        $sungokong = new Ape("kera sakti");
        echo "Name : $sungokong->name <br>"; 
        echo "Legs : $sungokong->legs <br>"; 
        echo "Cold Blooded : $sungokong->cold_blooded <br>";
        echo "Yell : " . $sungokong->yell() . "<br>"; // "Auooo"    
    ?>
    </div>
</body>
</html>


